# Generated by Django 4.1.6 on 2023-02-08 18:39

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_alter_customuser_activation_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='activation_code',
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
